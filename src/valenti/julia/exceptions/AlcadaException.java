
package valenti.julia.exceptions;


public abstract class AlcadaException extends Exception {
    
    public AlcadaException(String msg){
        super(msg);
    }

}
