
package valenti.julia.exceptions;


public class MassaBaixException extends AlcadaException {
    
    public MassaBaixException(String msg){
        super(msg);
    }

}
