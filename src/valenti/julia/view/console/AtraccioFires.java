package valenti.julia.view.console;

import valenti.julia.exceptions.AlcadaException;
import valenti.julia.model.PortaEntrada;
import valenti.julia.model.Sensor;

public class AtraccioFires {
    
    public static void main(String[] args) {
        int alcada;
        for(int i = 0; i < 20; i++){
            
            try{
                alcada = Sensor.obtenirAlcada();
                System.out.printf("SENSOR: Alçada llegida = %d%n", alcada);
                PortaEntrada.comprovar(alcada);
            }catch(AlcadaException e){
                System.out.printf("%s%n%n",e.getMessage());
            }
            
        }
        
    }

}
