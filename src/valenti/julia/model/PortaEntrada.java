package valenti.julia.model;

import valenti.julia.exceptions.MassaAltException;
import valenti.julia.exceptions.MassaBaixException;

public class PortaEntrada {

    public static final int ALCADA_MAX = 190;
    public static final int ALCADA_MIN = 150;

    public static void comprovar(int alcada) throws MassaAltException, MassaBaixException {

        if (alcada > ALCADA_MAX) {
            throw new MassaAltException("ERROR: alçada màxima de 190 sobrepassada");
        }
        
        if(alcada < ALCADA_MIN){
            throw new MassaBaixException("ERROR: no arriba a l'alçada mínima de 150");
        }
        
        System.out.printf("Obrint la porta a l'alçada %d cms%n%n", alcada);
        
        
    }

}
